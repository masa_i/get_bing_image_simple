# Image Getter #


```
#!sh
Usage: python bing_imagegetter_strict_allstyle.py query directory index_initnum max_num prefix

```

  
例）  
検索ワード：ビール  
出力先ディレクトリ：[OUTPUT val in script]/images 
画像の開始インデックス：0  
画像の取得最大数：1000  
画像名のprefix：beer  

```
#!sh
python bing_imagegetter_strict_allstyle.py 'ビール' images 0 1000 beer
```