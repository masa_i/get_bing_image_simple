#conding:utf-8

import sys
import os
import urllib
import urllib2
import json
import requests

KEY = 'MBHM0AwNnwxWx8Srii5PD+KgGC5KfkyyCEz+duiYCjU'
OUTPUT = '/Volumes/elements/sights_val/'
MAX = 1000
SKIP = 0
count = 1
index = 1

def bing_search(query, directory, skip, tag="normal"):
    global count
    global index
    bing_url = 'https://api.datamarket.azure.com/Bing/Search/Image'
    print 'search count: ' + str(count) + ', url: ' + bing_url + ', skip: ' + str(skip)
    pm = urllib2.HTTPPasswordMgrWithDefaultRealm()
    pm.add_password(None, bing_url, KEY, KEY)

    handler = urllib2.HTTPBasicAuthHandler(pm)
    opener = urllib2.build_opener(handler)
    urllib2.install_opener(opener)
    SKIP = skip
    if skip > 0:
        params = urllib.urlencode({'Query': "'" + query + "'", 'Adult': "'Strict'", '$skip': skip ,'$format': 'json'})
    else:    
        params = urllib.urlencode({'Query': "'" + query + "'", 'Adult': "'Strict'", '$format': 'json'})
    response = urllib2.urlopen(bing_url+'?'+params)
    data = json.loads(response.read())

    results = data['d']['results']

    print results
    
    for item in results:
        if count > MAX:
            print 'finish. count: ' + str(MAX)
            return

        image_url = item['MediaUrl']
        root,ext = os.path.splitext(image_url)
        if ext.lower() == '.jpg':
            print image_url,
            try:
                r = requests.get(image_url)

                if not r.status_code == 200:
                    continue    

                fname = OUTPUT + directory + "/" + tag + "_bing%04d.jpg" % index
                print fname
                f = open(fname, 'wb')
                f.write(r.content)
                f.close()

                try:
                    f2 = open(fname, 'r')
                except:
                    print "...delete"
                    f2.close()
                    os.remove(fname)
                print "...save", fname
            except:
                print "error."
            count += 1
            index += 1

    bing_search(query, directory, count, tag)

if __name__ == '__main__':
    argvs = sys.argv
    argc = len(argvs)
    if(argc != 6):
        print 'Usage: python %s query directory initnum max tag' % argvs[0]
        quit()
    query = argvs[1]
    directory = argvs[2]
    
    global index
    index = int(argvs[3])
    global MAX
    MAX = int(argvs[4])
    tag = argvs[5]

    print 'get bing image: %s ' % query
    bing_search(query, directory, 0, tag)
